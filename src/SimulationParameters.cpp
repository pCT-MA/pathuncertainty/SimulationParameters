#include <G4SystemOfUnits.hh>
#include <globals.hh>
#ifdef G4MULTITHREADED
#include <G4Threading.hh>
#endif

#include <pugixml.hpp>

#include <G4GeneralParticleSource.hh>
#include <G4NistManager.hh>

#include "SimulationParameters.hpp"

namespace Path
{
    bool IsMaterialName(std::string const& value)
    {
        auto nistManager = G4NistManager::Instance();
        return nistManager->FindOrBuildMaterial(value) != nullptr;
    }
    bool IsParticleName(std::string const& value)
    {
        for(const auto& name : gSupportedParticles)
        {
            if(name == value)
            {
                return true;
            }
        }
        return false;
    }

    // ---------------------------------------------------------------------- //

    BeamParameters::BeamParameters()
        : mParticleName(gProtonName), mSourceZ(0), mEnergy(200 * MeV),
          mFullWidthHalfMaximumInMM(0), mNumberOfPrimaries(100)
    {
    }

    bool BeamParameters::ParticleIsIon() const
    {
        return mParticleName != "proton" && mParticleName != "electron";
    }

    int BeamParameters::IonAtomicNumber() const
    {
        if(mParticleName == gProtonName)
            return 1;
        if(mParticleName == gHeliumName)
            return 2;
        return 0;
    }

    int BeamParameters::IonMassNumber() const
    {
        if(mParticleName == gProtonName)
            return 1;
        if(mParticleName == gHeliumName)
            return 4;
        return 0;
    }

    int BeamParameters::IonCharge() const { return IonAtomicNumber(); }

    double BeamParameters::RestEnergy() const
    {
        if(mParticleName == gElectronName)
            return 0.51099895000;
        if(mParticleName == gProtonName)
            return 938.2720881604904;
        if(mParticleName == gHeliumName)
            return 3727.379378;
        return 0;
    }

    bool BeamParameters::operator==(BeamParameters const& other) const
    {
        return mParticleName == other.mParticleName &&
               mSourceZ == other.mSourceZ && mEnergy == other.mEnergy &&
               mFullWidthHalfMaximumInMM == other.mFullWidthHalfMaximumInMM &&
               mNumberOfPrimaries == other.mNumberOfPrimaries;
    }

    // ---------------------------------------------------------------------- //

    VolumeParameters::VolumeParameters(std::string const& name,
                                       std::string const& material)
        : mName(name), mMaterial(material)
    {
    }

    bool VolumeParameters::operator==(VolumeParameters const& other) const
    {
        return mName == other.mName && mMaterial == other.mMaterial;
    }

    // ---------------------------------------------------------------------- //

    DetectorParameters::DetectorParameters()
        : VolumeParameters("Detector", gSiliciumMaterialName),
          mMaterialBudget(5e-3), mPlacementZ(0)
    {
    }

    float DetectorParameters::DetectorThickness() const
    {
        if(mMaterial == "G4_Galactic")

        {
            return 1 * micrometer;
        }
        auto NistManager = G4NistManager::Instance();
        auto detectorMaterial = NistManager->FindOrBuildMaterial(mMaterial);
        return std::max(1 * micrometer,
                        static_cast<double>(mMaterialBudget *
                                            detectorMaterial->GetRadlen()));
    }

    bool DetectorParameters::operator==(DetectorParameters const& other) const
    {
        return VolumeParameters::operator==(other) &&
               mMaterialBudget == other.mMaterialBudget &&
               mPlacementZ == other.mPlacementZ;
    }

    // ---------------------------------------------------------------------- //

    PhantomParameters::PhantomParameters()
        : VolumeParameters("Phantom", gWaterMaterialName), mThickness(200 * mm),
          mNumberOfLayers(100)
    {
    }

    float PhantomParameters::LayerThickness() const
    {
        return static_cast<float>(static_cast<double>(mThickness) /
                                  static_cast<double>(mNumberOfLayers));
    }

    bool PhantomParameters::operator==(PhantomParameters const& other) const
    {
        return VolumeParameters::operator==(other) &&
               mThickness == other.mThickness &&
               mNumberOfLayers == other.mNumberOfLayers;
    }

    // ---------------------------------------------------------------------- //

    WorldParameters::WorldParameters()
        : VolumeParameters("World", gGalacticMaterialName), mWorldSize(500 * mm)
    {
    }

    bool WorldParameters::operator==(WorldParameters const& other) const
    {
        return VolumeParameters::operator==(other) &&
               mWorldSize == other.mWorldSize;
    }

    // ---------------------------------------------------------------------- //

    SimulationParameters::SimulationParameters()
        : mSimulationName("simulation"), mOutDirectoryName("."),
          mOnlyEMPhysics(false)
#ifdef G4MULTITHREADED
          ,
          mNumberOfThreads(1)
#endif
    {
    }

    std::string SimulationParameters::SimulationFileName() const
    {
        return mOutDirectoryName + "/" + mSimulationName + ".root";
    }

    bool
    SimulationParameters::operator==(SimulationParameters const& other) const
    {
        return mSimulationName == other.mSimulationName &&
               mOutDirectoryName == other.mOutDirectoryName &&
               mOnlyEMPhysics == other.mOnlyEMPhysics
#ifdef G4MULTITHREADED
               && mNumberOfThreads == other.mNumberOfThreads
#endif
                ;
    }

    // ---------------------------------------------------------------------- //

    bool ParameterSet::operator==(ParameterSet const& other) const
    {
        return mBeamParameters == other.mBeamParameters &&
               mDetectorParameters == other.mDetectorParameters &&
               mPhantomParameters == other.mPhantomParameters &&
               mWorldParameters == other.mWorldParameters &&
               mSimulationParameters == other.mSimulationParameters;
    }

    void ThrowInvalid(std::string const& message)
    {
        throw std::invalid_argument(message);
    }

    template <typename T>
    T Convert(std::string const& str, std::string const& var)
    {
        T result;
        std::stringstream convert(str);
        convert >> std::noskipws >> result;
        if(!convert.eof() || convert.fail())
        {
            ThrowInvalid(var + " is not numeric");
        }
        return result;
    }

    bool MatchesKey(pugi::xml_node const& node, std::string const& key)
    {
        return node.name() == key;
    }

    template <typename T>
    bool TryParse(pugi::xml_node const& node,
                  std::string const& key,
                  T& returnValue,
                  bool (*check)(T),
                  std::string const& checkFailMessage)
    {
        if(MatchesKey(node, key))
        {
            const auto value = Convert<T>(node.child_value(), key);
            if(!check(value))
            {
                ThrowInvalid(checkFailMessage);
            }
            returnValue = value;
            return true;
        }
        return false;
    }

    template <typename T> bool AtLeast(T t, T t0) { return t >= t0; }
    template <typename T> bool NonNegative(T t) { return AtLeast<T>(t, 0); }
    template <typename T> bool AlwaysTrue(T) { return true; }

    BeamParameters ParseBeam(pugi::xml_node const& node)
    {
        BeamParameters parameters;

        for(const auto& child : node.children())
        {
            bool didParse = false;
            didParse |= TryParse<float>(child,
                                        "Energy",
                                        parameters.mEnergy,
                                        NonNegative,
                                        "Energy is less than 0");
            didParse |= TryParse<float>(child,
                                        "FWHM",
                                        parameters.mFullWidthHalfMaximumInMM,
                                        NonNegative,
                                        "FWHM is less than 0");

            long long numberOfPrimaries = 0;
            if(TryParse<long long>(child,
                                   "NumberOfPrimaries",
                                   numberOfPrimaries,
                                   NonNegative,
                                   "NumberOfPrimaries is less than 0"))
            {
                parameters.mNumberOfPrimaries =
                        static_cast<std::size_t>(numberOfPrimaries);
                didParse = true;
            }

            if(MatchesKey(child, "SourceZ"))
            {
                parameters.mSourceZ =
                        Convert<float>(child.child_value(), "SourceZ");
                didParse = true;
            }

            if(MatchesKey(child, "ParticleName"))
            {
                bool supported = false;

                for(auto name : gSupportedParticles)
                {
                    if(std::string(name) == child.child_value())
                    {
                        supported = true;
                    }
                }
                if(!supported)
                {
                    ThrowInvalid("Unsupported particle name");
                }
                parameters.mParticleName = child.child_value();
                didParse = true;
            }

            if(!didParse)
            {
                ThrowInvalid("Unknown key: " + std::string(child.name()));
            }
        }
        return parameters;
    }

    bool ValidMaterial(std::string const& name)
    {
        auto nistManager = G4NistManager::Instance();
        auto material = nistManager->FindOrBuildMaterial(name);
        return material != nullptr;
    }

    bool ParseNonEmptyString(pugi::xml_node const& node,
                             std::string const& key,
                             std::string& result)
    {
        if(MatchesKey(node, key))
        {
            if(std::string(node.child_value()).empty())
            {
                ThrowInvalid("Empty " + key);
            }
            result = node.child_value();
            return true;
        }
        return false;
    }

    bool ParseVolume(pugi::xml_node const& node, VolumeParameters& result)
    {
        bool didParse = ParseNonEmptyString(node, "Name", result.mName);

        if(MatchesKey(node, "Material"))
        {
            if(std::string(node.child_value()).empty())
            {
                ThrowInvalid("Empty material");
            }
            if(!ValidMaterial(node.child_value()))
            {
                ThrowInvalid("Unknown material");
            }
            didParse = true;
            result.mMaterial = node.child_value();
        }
        return didParse;
    }

    DetectorParameters ParseDetectors(pugi::xml_node const& node)
    {
        DetectorParameters result;
        for(const auto& child : node.children())
        {
            bool didParse = ParseVolume(child, result);

            didParse |= TryParse<float>(child,
                                        "MaterialBudget",
                                        result.mMaterialBudget,
                                        NonNegative,
                                        "MaterialBudget is less than 0");

            didParse |= TryParse<float>(
                    child, "PlacementZ", result.mPlacementZ, AlwaysTrue, "");

            if(!didParse)
            {
                ThrowInvalid("Unknown key: " + std::string(child.name()));
            }
        }
        return result;
    }

    PhantomParameters ParsePhantom(pugi::xml_node const& node)
    {
        PhantomParameters result;
        for(const auto& child : node.children())
        {
            bool didParse = ParseVolume(child, result);

            didParse |= TryParse<float>(child,
                                        "Thickness",
                                        result.mThickness,
                                        NonNegative,
                                        "Thickness is less than 0");

            long long numberOfLayers = 0;
            if(TryParse<long long>(child,
                                   "NumberOfLayers",
                                   numberOfLayers,
                                   NonNegative,
                                   "NumberOfLayers is less than 0"))
            {
                result.mNumberOfLayers =
                        static_cast<std::size_t>(numberOfLayers);
                didParse = true;
            }

            if(!didParse)
            {
                ThrowInvalid("Unknown key: " + std::string(child.name()));
            }
        }
        return result;
    }

    WorldParameters ParseWorld(pugi::xml_node const& node)
    {
        WorldParameters result;
        for(const auto& child : node.children())
        {
            bool didParse = ParseVolume(child, result) ||
                            TryParse<float>(child,
                                            "WorldSize",
                                            result.mWorldSize,
                                            NonNegative,
                                            "WorldSize is less than 0");

            if(!didParse)
            {
                ThrowInvalid("Unknown key: " + std::string(child.name()));
            }
        }
        return result;
    }

    SimulationParameters ParseSimulation(pugi::xml_node const& node)
    {
        SimulationParameters result;
        for(const auto& child : node.children())
        {
            bool didParse =
                    ParseNonEmptyString(child, "Name", result.mSimulationName);
            didParse |= ParseNonEmptyString(
                    child, "OutputDirectory", result.mOutDirectoryName);

#ifdef G4MULTITHREADED
            int numberOfThreads;
            bool parsedNumberOfThreads = TryParse<int>(
                    child,
                    "NumberOfThreads",
                    numberOfThreads,
                    [](int v) { return AtLeast(v, 1); },
                    "NumberOfThreads is less than 1");
            if(parsedNumberOfThreads)
            {
                result.mNumberOfThreads =
                        std::min(static_cast<std::size_t>(numberOfThreads),
                                 static_cast<std::size_t>(
                                         G4Threading::G4GetNumberOfCores()));
                didParse = true;
            }
#endif
            if(MatchesKey(child, "OnlyEmPhysics"))
            {
                didParse = true;
                result.mOnlyEMPhysics = true;
            }

            if(!didParse)
            {
                ThrowInvalid("Unknown key: " + std::string(child.name()));
            }
        }
        return result;
    }

    ParameterSet XMLParameters(std::string const& fileName)
    {
        ParameterSet result;
        pugi::xml_document doc;
        auto load_result = doc.load_file(fileName.c_str());
        if(load_result.status != pugi::status_ok)
            ThrowInvalid("Error parsing file: \"" + fileName + "\"");

        auto root = doc.first_child();
        if(root.empty())
        {
            return result;
        }
        if(!MatchesKey(root, "ParameterSet"))
        {
            throw std::invalid_argument("Root node isn't called ParameterSet.");
        }

        for(const auto& child : root.children())
        {
            if(MatchesKey(child, "BeamParameters"))
            {
                result.mBeamParameters = ParseBeam(child);
            }
            else if(MatchesKey(child, "DetectorParameters"))
            {
                auto detector = ParseDetectors(child);

                result.mDetectorParameters.push_back(detector);
            }
            else if(MatchesKey(child, "PhantomParameters"))
            {
                result.mPhantomParameters = ParsePhantom(child);
            }
            else if(MatchesKey(child, "WorldParameters"))
            {
                result.mWorldParameters = ParseWorld(child);
            }
            else if(MatchesKey(child, "SimulationParameters"))
            {
                result.mSimulationParameters = ParseSimulation(child);
            }
            else
            {
                ThrowInvalid("Unknown key: " + std::string(child.name()));
            }
        }

        return result;
    }

} // namespace Path
