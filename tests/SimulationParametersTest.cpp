#include "doctest/doctest.h"

#include <G4SystemOfUnits.hh>

#include "SimulationParameters.hpp"

template <typename T> bool NearlyEqual(T a, T b, T precision = 1e-8)
{
    return std::abs(a - b) < precision;
}

TEST_CASE("Parameters")
{
    using namespace Path;

    SUBCASE("BeamParameters")
    {
        BeamParameters parameters;

        SUBCASE("Default Name")
        {
            CHECK(parameters.mParticleName == gProtonName);
        }
        SUBCASE("Default Source Z") { CHECK(parameters.mSourceZ == 0); }
        SUBCASE("Default Energy") { CHECK(parameters.mEnergy == 200 * MeV); }
        SUBCASE("Default FWHM")
        {
            CHECK(parameters.mFullWidthHalfMaximumInMM == 0);
        }
        SUBCASE("Default number of primaries")
        {
            CHECK(parameters.mNumberOfPrimaries == 100);
        }

        SUBCASE("Particle Is Ion")
        {
            auto saved = parameters.mParticleName;

            SUBCASE("Electron")
            {
                parameters.mParticleName = gElectronName;
                CHECK(false == parameters.ParticleIsIon());
            }

            SUBCASE("Proton")
            {
                parameters.mParticleName = gProtonName;
                CHECK(false == parameters.ParticleIsIon());
            }

            SUBCASE("Helium")
            {
                parameters.mParticleName = gHeliumName;
                CHECK(true == parameters.ParticleIsIon());
            }

            parameters.mParticleName = saved;
        }

        SUBCASE("Atomic Number")
        {
            auto saved = parameters.mParticleName;

            SUBCASE("Electron")
            {
                parameters.mParticleName = gElectronName;
                CHECK(0 == parameters.IonAtomicNumber());
            }

            SUBCASE("Proton")
            {
                parameters.mParticleName = gProtonName;
                CHECK(1 == parameters.IonAtomicNumber());
            }

            SUBCASE("Helium")
            {
                parameters.mParticleName = gHeliumName;
                CHECK(2 == parameters.IonAtomicNumber());
            }

            parameters.mParticleName = saved;
        }

        SUBCASE("Ion Mass Number")
        {
            auto saved = parameters.mParticleName;

            SUBCASE("Electron")
            {
                parameters.mParticleName = gElectronName;
                CHECK(0 == parameters.IonMassNumber());
            }

            SUBCASE("Proton")
            {
                parameters.mParticleName = gProtonName;
                CHECK(1 == parameters.IonMassNumber());
            }

            SUBCASE("Helium")
            {
                parameters.mParticleName = gHeliumName;
                CHECK(4 == parameters.IonMassNumber());
            }

            parameters.mParticleName = saved;
        }

        SUBCASE("Ion Charge")
        {
            auto saved = parameters.mParticleName;

            SUBCASE("Electron")
            {
                parameters.mParticleName = gElectronName;
                CHECK(0 == parameters.IonCharge());
            }

            SUBCASE("Proton")
            {
                parameters.mParticleName = gProtonName;
                CHECK(1 == parameters.IonCharge());
            }

            SUBCASE("Helium")
            {
                parameters.mParticleName = gHeliumName;
                CHECK(2 == parameters.IonCharge());
            }

            parameters.mParticleName = saved;
        }

        SUBCASE("Rest energy")
        {
            auto saved = parameters.mParticleName;

            SUBCASE("Electron")
            {
                parameters.mParticleName = gElectronName;
                CHECK(0.51099895000 == parameters.RestEnergy());
            }

            SUBCASE("Proton")
            {
                parameters.mParticleName = gProtonName;
                CHECK(938.2720881604904 == parameters.RestEnergy());
            }

            SUBCASE("Helium")
            {
                parameters.mParticleName = gHeliumName;
                CHECK(3727.379378 == parameters.RestEnergy());
            }

            parameters.mParticleName = saved;
        }

        SUBCASE("Operator==")
        {
            SUBCASE("Values Match")
            {
                BeamParameters b;
                CHECK(parameters == b);
            }

            SUBCASE("Name Mismatch")
            {
                BeamParameters b;
                b.mParticleName = gHeliumName;

                CHECK(false == (parameters == b));
            }

            SUBCASE("SourceZ Mismatch")
            {
                BeamParameters b;
                b.mSourceZ = 100;

                CHECK(false == (parameters == b));
            }

            SUBCASE("Energy Mismatch")
            {
                BeamParameters b;
                b.mEnergy = 100;

                CHECK(false == (parameters == b));
            }

            SUBCASE("FWHM Mismatch")
            {
                BeamParameters b;
                b.mFullWidthHalfMaximumInMM = 100;

                CHECK(false == (parameters == b));
            }

            SUBCASE("Number of primaries Mismatch")
            {
                BeamParameters b;
                b.mNumberOfPrimaries = 200;

                CHECK(false == (parameters == b));
            }
        }
    }

    SUBCASE("DetectorParameters")
    {
        DetectorParameters parameters;

        SUBCASE("Default Name") { CHECK(parameters.mName == "Detector"); }
        SUBCASE("Default Material")
        {
            CHECK(parameters.mMaterial == gSiliciumMaterialName);
        }
        SUBCASE("Default Material Budget")
        {
            CHECK(parameters.mMaterialBudget == 5e-3f);
        }
        SUBCASE("Default Placement Z") { CHECK(parameters.mPlacementZ == 0); }

        SUBCASE("DetectorThickness()")
        {
            CHECK(NearlyEqual(
                    parameters.DetectorThickness(), 93.70f * 0.005f, 1e-3f));
        }

        SUBCASE("Operator==")
        {
            SUBCASE("Values Match")
            {
                DetectorParameters b;
                CHECK(parameters == b);
            }

            SUBCASE("Name Mismatch")
            {
                DetectorParameters b;
                b.mName = "OtherName";

                CHECK(false == (parameters == b));
            }

            SUBCASE("Material Mismatch")
            {
                DetectorParameters b;
                b.mMaterial = gWaterMaterialName;

                CHECK(false == (parameters == b));
            }

            SUBCASE("MaterialBudget Mismatch")
            {
                DetectorParameters b;
                b.mMaterialBudget = 1;

                CHECK(false == (parameters == b));
            }

            SUBCASE("PlacementZ Mismatch")
            {
                DetectorParameters b;
                b.mPlacementZ = 10;

                CHECK(false == (parameters == b));
            }
        }
    }

    SUBCASE("PhantomParameters")
    {
        PhantomParameters parameters;

        SUBCASE("Default Name") { CHECK(parameters.mName == "Phantom"); }
        SUBCASE("Default Material")
        {
            CHECK(parameters.mMaterial == gWaterMaterialName);
        }
        SUBCASE("Default Thickness")
        {
            CHECK(parameters.mThickness == 200 * mm);
        }
        SUBCASE("Default number of layers")
        {
            CHECK(parameters.mNumberOfLayers == 100);
        }

        SUBCASE("Layer Thickness")
        {
            CHECK(parameters.LayerThickness() == 200 * mm / 100);
        }

        SUBCASE("Operator==")
        {
            SUBCASE("Values Match")
            {
                PhantomParameters b;
                CHECK(parameters == b);
            }

            SUBCASE("Name Mismatch")
            {
                PhantomParameters b;
                b.mName = "OtherName";

                CHECK(false == (parameters == b));
            }

            SUBCASE("Material Mismatch")
            {
                PhantomParameters b;
                b.mMaterial = gSiliciumMaterialName;

                CHECK(false == (parameters == b));
            }

            SUBCASE("Thickness Mismatch")
            {
                PhantomParameters b;
                b.mThickness = 20;

                CHECK(false == (parameters == b));
            }

            SUBCASE("Number of layers Mismatch")
            {
                PhantomParameters b;
                b.mNumberOfLayers = 20;

                CHECK(false == (parameters == b));
            }
        }
    }

    SUBCASE("WorldParameters")
    {
        WorldParameters parameters;

        SUBCASE("Default Name") { CHECK(parameters.mName == "World"); }
        SUBCASE("Default Material")
        {
            CHECK(parameters.mMaterial == gGalacticMaterialName);
        }
        SUBCASE("Default Size") { CHECK(parameters.mWorldSize == 500 * mm); }

        SUBCASE("Operator==")
        {
            SUBCASE("Values Match")
            {
                WorldParameters b;
                CHECK(parameters == b);
            }

            SUBCASE("Name Mismatch")
            {
                WorldParameters b;
                b.mName = "OtherName";

                CHECK(false == (parameters == b));
            }

            SUBCASE("Material Mismatch")
            {
                WorldParameters b;
                b.mMaterial = gSiliciumMaterialName;

                CHECK(false == (parameters == b));
            }

            SUBCASE("Size Mismatch")
            {
                WorldParameters b;
                b.mWorldSize = 20;

                CHECK(false == (parameters == b));
            }
        }
    }

    SUBCASE("SimulationParameters")
    {
        SimulationParameters parameters;

        SUBCASE("Default Simulation Name")
        {
            CHECK(parameters.mSimulationName == "simulation");
        }
        SUBCASE("Default Output Directory")
        {
            CHECK(parameters.mOutDirectoryName == ".");
        }
        SUBCASE("Default Only EM")
        {
            CHECK(parameters.mOnlyEMPhysics == false);
        }
#ifdef G4MULTITHREADED
        SUBCASE("Default Number of Threads")
        {
            CHECK(parameters.mNumberOfThreads == 1);
        }
#endif
        SUBCASE("SimulationFileName()")
        {
            CHECK(parameters.SimulationFileName() == "./simulation.root");
        }

        SUBCASE("Operator==")
        {
            SUBCASE("Values Match")
            {
                SimulationParameters b;
                CHECK(parameters == b);
            }

            SUBCASE("Name mismatch")
            {
                SimulationParameters b;
                b.mSimulationName = "OtherName";

                CHECK(false == (parameters == b));
            }

            SUBCASE("OutDirectory mismatch")
            {
                SimulationParameters b;
                b.mOutDirectoryName = "OtherName";

                CHECK(false == (parameters == b));
            }

            SUBCASE("Only Em Physics mismatch")
            {
                SimulationParameters b;
                b.mOnlyEMPhysics = !parameters.mOnlyEMPhysics;

                CHECK(false == (parameters == b));
            }

#ifdef G4MULTITHREADED
            SUBCASE("Number of threads mismatch")
            {
                SimulationParameters b;
                b.mNumberOfThreads = 33;

                CHECK(false == (parameters == b));
            }
#endif
        }
    }

    SUBCASE("ParameterSet")
    {
        ParameterSet parameters;

        SUBCASE("BeamParameters")
        {
            BeamParameters beamParameters;

            CHECK(beamParameters == parameters.mBeamParameters);
        }

        SUBCASE("DetectorParameters")
        {
            CHECK(parameters.mDetectorParameters.empty());
        }

        SUBCASE("PhantomParameters")
        {
            PhantomParameters phantomParameters;

            CHECK(parameters.mPhantomParameters == phantomParameters);
        }

        SUBCASE("WorldParameters")
        {
            WorldParameters worldParameters;

            CHECK(parameters.mWorldParameters == worldParameters);
        }

        SUBCASE("SimulationParameters")
        {
            SimulationParameters simulationParameters;

            CHECK(parameters.mSimulationParameters == simulationParameters);
        }
        SUBCASE("Operator==")
        {
            SUBCASE("Values Match")
            {
                ParameterSet b;

                CHECK(parameters == b);
            }

            SUBCASE("BeamParameters Mismatch")
            {
                ParameterSet b;
                b.mBeamParameters.mNumberOfPrimaries = 123;

                CHECK(false == (parameters == b));
            }

            SUBCASE("DetectorParameters Mismatch")
            {
                ParameterSet b;
                b.mDetectorParameters.push_back({});

                CHECK(false == (parameters == b));
            }

            SUBCASE("PhantomParameters Mismatch")
            {
                ParameterSet b;
                b.mPhantomParameters.mNumberOfLayers = 123;

                CHECK(false == (parameters == b));
            }

            SUBCASE("WorldParameters Mismatch")
            {
                ParameterSet b;
                b.mWorldParameters.mWorldSize = 123;

                CHECK(false == (parameters == b));
            }

            SUBCASE("SimulationParameters Mismatch")
            {
                ParameterSet b;
                b.mSimulationParameters.mSimulationName = "OtherName";

                CHECK(false == (parameters == b));
            }
        }
    }
}
