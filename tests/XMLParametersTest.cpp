#include <algorithm> // toupper

#include "doctest/doctest.h"

#ifdef G4MULTITHREADED
#include <G4Threading.hh>
#endif

#include "SimulationParameters.hpp"

TEST_CASE("XMLParametersTest")
{
    using namespace Path;

    BeamParameters defaultBeam;
    DetectorParameters defaultDetector;
    PhantomParameters defaultPhantom;
    WorldParameters defaultWorld;
    SimulationParameters defaultSimulation;
    ParameterSet defaultSet;

    SUBCASE("File does not exist")
    {
        CHECK_THROWS_AS(XMLParameters(""), std::invalid_argument);
    }

    SUBCASE("Empty")
    {
        CHECK_THROWS_AS(XMLParameters("Empty.xml"), std::invalid_argument);
    }

    SUBCASE("File contains something else")
    {
        CHECK_THROWS_AS(XMLParameters("SomethingElse.xml"), std::invalid_argument);
    }

    SUBCASE("Root node is called ParameterSet")
    {
        CHECK_THROWS_AS_MESSAGE(XMLParameters("WrongRootNode.xml"),
                                std::invalid_argument,
                                "Root node isn't called ParameterSet.");
    }

    SUBCASE("Beam parameter")
    {
        SUBCASE("Energy is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_EnergyIsntNumeric.xml"),
                    std::invalid_argument,
                    "Energy is not numeric");
        }

        SUBCASE("Energy is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_EnergyIsLessThanZero.xml"),
                    std::invalid_argument,
                    "Energy is less than 0");
        }

        SUBCASE("Energy is valid number")
        {
            BeamParameters expectedParameters;
            expectedParameters.mEnergy = 100;
            auto parameters =
                    XMLParameters("BeamParameters_EnergyIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mBeamParameters);
        }

        SUBCASE("FWHM is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_FwhmIsntNumeric.xml"),
                    std::invalid_argument,
                    "FWHM is not numeric");
        }

        SUBCASE("FWHM is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_FwhmIsLessThanZero.xml"),
                    std::invalid_argument,
                    "FWHM is less than 0");
        }

        SUBCASE("FWHM is valid number")
        {
            BeamParameters expectedParameters;
            expectedParameters.mFullWidthHalfMaximumInMM = 100;
            auto parameters =
                    XMLParameters("BeamParameters_FwhmIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mBeamParameters);
        }

        SUBCASE("Number of primaries is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "BeamParameters_NumberOfPrimariesIsntNumeric.xml"),
                    std::invalid_argument,
                    "NumberOfPrimaries is not numeric");
        }

        SUBCASE("Number of primaries is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_"
                                  "NumberOfPrimariesIsLessThanZero.xml"),
                    std::invalid_argument,
                    "NumberOfPrimaries is less than 0");
        }

        SUBCASE("Number of primaries is valid number")
        {
            BeamParameters expectedParameters;
            expectedParameters.mNumberOfPrimaries = 100;
            auto parameters = XMLParameters(
                    "BeamParameters_NumberOfPrimariesIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mBeamParameters);
        }

        SUBCASE("Source Z is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_SourceZIsntNumeric.xml"),
                    std::invalid_argument,
                    "Source Z is not numeric");
        }

        SUBCASE("Source Z is valid number")
        {
            BeamParameters expectedParameters;
            expectedParameters.mSourceZ = 100;
            auto parameters =
                    XMLParameters("BeamParameters_SourceZIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mBeamParameters);
        }

        SUBCASE("Particle name isn't unsupported")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "BeamParameters_ParticleNameIsntSupported.xml"),
                    std::invalid_argument,
                    "Unsupported particle name");
        }

        SUBCASE("Supported particles are supported")
        {
            for(auto name : gSupportedParticles)
            {
                BeamParameters expectedParameters;
                expectedParameters.mParticleName = name;
                std::string first = std::string(name).substr(0, 1);
                std::transform(
                        first.begin(), first.end(), first.begin(), ::toupper);

                auto parameters =
                        XMLParameters("BeamParameters_" + first +
                                      std::string(name).substr(1) + ".xml");

                CHECK(expectedParameters == parameters.mBeamParameters);
            }
        }

        SUBCASE("Complete example")
        {
            BeamParameters expectedParameters;
            expectedParameters.mEnergy = 1;
            expectedParameters.mFullWidthHalfMaximumInMM = 2;
            expectedParameters.mNumberOfPrimaries = 3;
            expectedParameters.mSourceZ = 4;
            expectedParameters.mParticleName = "electron";
            auto parseResult = XMLParameters("BeamParameters.xml");

            CHECK(expectedParameters == parseResult.mBeamParameters);
        }

        SUBCASE("Unknown key")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("BeamParameters_UnknownKey.xml"),
                    std::invalid_argument,
                    "Unknown key: FullWidthHalfMaximum");
        }

        SUBCASE("Others are kept at default")
        {
            auto parseResult = XMLParameters("BeamParameters.xml");

            CHECK(parseResult.mDetectorParameters.empty());
            CHECK(parseResult.mPhantomParameters == defaultPhantom);
            CHECK(parseResult.mWorldParameters == defaultWorld);
            CHECK(parseResult.mSimulationParameters == defaultSimulation);
        }
    }

    SUBCASE("DetectorParameters")
    {
        SUBCASE("PlacementZ is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "DetectorParameters_PlacementZIsntNumeric.xml"),
                    std::invalid_argument,
                    "PlacementZ is not numeric");
        }

        SUBCASE("PlacementZ is valid number")
        {
            DetectorParameters expectedParameters;
            expectedParameters.mPlacementZ = -10.5;
            auto parameters = XMLParameters(
                    "DetectorParameters_PlacementZIsValidNumber.xml");

            REQUIRE(1 == parameters.mDetectorParameters.size());
            CHECK(expectedParameters == parameters.mDetectorParameters.front());
        }

        SUBCASE("Material budget is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "DetectorParameters_MaterialBudgetIsntNumeric.xml"),
                    std::invalid_argument,
                    "MaterialBudget is not numeric");
        }

        SUBCASE("Material budget is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("DetectorParameters_"
                                  "MaterialBudgetIsLessThanZero.xml"),
                    std::invalid_argument,
                    "MaterialBudget is less than 0");
        }

        SUBCASE("Material budget is valid number")
        {
            DetectorParameters expectedParameters;
            expectedParameters.mMaterialBudget = 0.005;
            auto parameters = XMLParameters(
                    "DetectorParameters_MaterialBudgetIsValidNumber.xml");

            REQUIRE(1 == parameters.mDetectorParameters.size());
            CHECK(expectedParameters == parameters.mDetectorParameters.front());
        }

        SUBCASE("Name is not empty")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("DetectorParameters_EmptyName.xml"),
                    std::invalid_argument,
                    "Empty name");
        }

        SUBCASE("Name is accepted")
        {
            DetectorParameters expectedParameters;
            expectedParameters.mName = "Name";
            auto parameters = XMLParameters("DetectorParameters_Name.xml");

            REQUIRE(1 == parameters.mDetectorParameters.size());
            CHECK(expectedParameters == parameters.mDetectorParameters.front());
        }

        SUBCASE("Material is not known by Geant4")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("DetectorParameters_UnknownMaterial.xml"),
                    std::invalid_argument,
                    "Unknown material");
        }

        SUBCASE("Material is known by Geant4")
        {
            DetectorParameters expectedParameters;
            expectedParameters.mMaterial = gWaterMaterialName;
            auto parameters =
                    XMLParameters("DetectorParameters_ValidMaterial.xml");

            REQUIRE(1 == parameters.mDetectorParameters.size());
            CHECK(expectedParameters == parameters.mDetectorParameters.front());
        }

        SUBCASE("Unknown key")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("DetectorParameters_UnknownKey.xml"),
                    std::invalid_argument,
                    "Unknown key: FullWidthHalfMaximum");
        }

        SUBCASE("Complete example")
        {
            ParameterSet expectedParameters;
            std::array<float, 4> locations = {-200, -100, 300, 400};
            for(std::size_t i = 0; i < 3; i++)
            {
                for(std::size_t j = 0; j < 1; j++)
                {
                    DetectorParameters newDetector;
                    newDetector.mName =
                            "A" + std::to_string(i) + (j == 0 ? "x" : "y");
                    newDetector.mMaterial = gWaterMaterialName;
                    newDetector.mMaterialBudget = 0.01;
                    newDetector.mPlacementZ =
                            locations.at(i) + (j == 0 ? -1 : 1);

                    expectedParameters.mDetectorParameters.push_back(
                            newDetector);
                }
            }

            auto parseResult = XMLParameters("DetectorParameters.xml");
        }

        SUBCASE("Others are kept at default")
        {
            auto parseResult = XMLParameters("DetectorParameters.xml");

            CHECK(parseResult.mBeamParameters == defaultBeam);
            CHECK(parseResult.mPhantomParameters == defaultPhantom);
            CHECK(parseResult.mWorldParameters == defaultWorld);
            CHECK(parseResult.mSimulationParameters == defaultSimulation);
        }
    }

    SUBCASE("PhantomParameters")
    {
        SUBCASE("Thickness is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("PhantomParameters_ThicknessIsntNumeric.xml"),
                    std::invalid_argument,
                    "Thickness is not numeric");
        }

        SUBCASE("Thickness is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "PhantomParameters_ThicknessIsLessThanZero.xml"),
                    std::invalid_argument,
                    "Thickness is less than 0");
        }

        SUBCASE("Thickness is valid number")
        {
            PhantomParameters expectedParameters;
            expectedParameters.mThickness = 100;
            auto parameters = XMLParameters(
                    "PhantomParameters_ThicknessIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mPhantomParameters);
        }

        SUBCASE("Number of layers is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "PhantomParameters_NumberOfLayersIsntNumeric.xml"),
                    std::invalid_argument,
                    "NumberOfLayers is not numeric");
        }

        SUBCASE("NumberOfLayers is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("PhantomParameters_"
                                  "NumberOfLayersIsLessThanZero.xml"),
                    std::invalid_argument,
                    "NumberOfLayers is less than 0");
        }

        SUBCASE("NumberOfLayers is valid number")
        {
            PhantomParameters expectedParameters;
            expectedParameters.mNumberOfLayers = 50;
            auto parameters = XMLParameters(
                    "PhantomParameters_NumberOfLayersIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mPhantomParameters);
        }

        SUBCASE("Name is not empty")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("PhantomParameters_EmptyName.xml"),
                    std::invalid_argument,
                    "Empty name");
        }

        SUBCASE("Name is accepted")
        {
            PhantomParameters expectedParameters;
            expectedParameters.mName = "Name";
            auto parameters = XMLParameters("PhantomParameters_Name.xml");

            CHECK(expectedParameters == parameters.mPhantomParameters);
        }

        SUBCASE("Material is not known by Geant4")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("PhantomParameters_UnknownMaterial.xml"),
                    std::invalid_argument,
                    "Unknown material");
        }

        SUBCASE("Material is known by Geant4")
        {
            PhantomParameters expectedParameters;
            expectedParameters.mMaterial = gSiliciumMaterialName;
            auto parameters =
                    XMLParameters("PhantomParameters_ValidMaterial.xml");

            CHECK(expectedParameters == parameters.mPhantomParameters);
        }

        SUBCASE("Unknown key")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("PhantomParameters_UnknownKey.xml"),
                    std::invalid_argument,
                    "Unknown key: FullWidthHalfMaximum");
        }

        SUBCASE("Complete example")
        {
            PhantomParameters expectedParameters;
            expectedParameters.mName = "OtherName";
            expectedParameters.mMaterial = gSiliciumMaterialName;
            expectedParameters.mNumberOfLayers = 10;
            expectedParameters.mThickness = 5;
            auto parameters = XMLParameters("PhantomParameters.xml");

            CHECK(expectedParameters == parameters.mPhantomParameters);
        }

        SUBCASE("Others are kept at default")
        {
            auto parseResult = XMLParameters("PhantomParameters.xml");

            CHECK(parseResult.mBeamParameters == defaultBeam);
            CHECK(parseResult.mDetectorParameters.empty());
            CHECK(parseResult.mWorldParameters == defaultWorld);
            CHECK(parseResult.mSimulationParameters == defaultSimulation);
        }
    }

    SUBCASE("WorldParameters")
    {
        SUBCASE("Name is not empty")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("WorldParameters_EmptyName.xml"),
                    std::invalid_argument,
                    "Empty name");
        }

        SUBCASE("Name is accepted")
        {
            WorldParameters expectedParameters;
            expectedParameters.mName = "Name";
            auto parameters = XMLParameters("WorldParameters_Name.xml");

            CHECK(expectedParameters == parameters.mWorldParameters);
        }

        SUBCASE("Material is not known by Geant4")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("WorldParameters_UnknownMaterial.xml"),
                    std::invalid_argument,
                    "Unknown material");
        }

        SUBCASE("Material is known by Geant4")
        {
            WorldParameters expectedParameters;
            expectedParameters.mMaterial = gWaterMaterialName;
            auto parameters =
                    XMLParameters("WorldParameters_ValidMaterial.xml");

            CHECK(expectedParameters == parameters.mWorldParameters);
        }

        SUBCASE("WorldSize is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("WorldParameters_WorldSizeIsntNumeric.xml"),
                    std::invalid_argument,
                    "WorldSize is not numeric");
        }

        SUBCASE("WorldSize is ≥ 0")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("WorldParameters_"
                                  "WorldSizeIsLessThanZero.xml"),
                    std::invalid_argument,
                    "WorldSize is less than 0");
        }

        SUBCASE("WorldSize is valid number")
        {
            WorldParameters expectedParameters;
            expectedParameters.mWorldSize = 100;
            auto parameters =
                    XMLParameters("WorldParameters_WorldSizeIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mWorldParameters);
        }

        SUBCASE("Unknown key")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("WorldParameters_UnknownKey.xml"),
                    std::invalid_argument,
                    "Unknown key: FullWidthHalfMaximum");
        }

        SUBCASE("Complete example")
        {
            WorldParameters expectedParameters;
            expectedParameters.mWorldSize = 100;
            expectedParameters.mName = "OtherName";
            expectedParameters.mMaterial = gSiliciumMaterialName;
            auto parseResult = XMLParameters("WorldParameters.xml");

            CHECK(expectedParameters == parseResult.mWorldParameters);
        }

        SUBCASE("Others are kept at default")
        {
            auto parseResult = XMLParameters("WorldParameters.xml");

            CHECK(parseResult.mBeamParameters == defaultBeam);
            CHECK(parseResult.mPhantomParameters == defaultPhantom);
            CHECK(parseResult.mDetectorParameters.empty());
            CHECK(parseResult.mSimulationParameters == defaultSimulation);
        }
    }

    SUBCASE("SimulationParameters")
    {
        SUBCASE("Name is not empty")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("SimulationParameters_EmptyName.xml"),
                    std::invalid_argument,
                    "Empty name");
        }

        SUBCASE("Name is accepted")
        {
            SimulationParameters expectedParameters;
            expectedParameters.mSimulationName = "Name";
            auto parameters = XMLParameters("SimulationParameters_Name.xml");

            CHECK(expectedParameters == parameters.mSimulationParameters);
        }

        SUBCASE("OutputDirectory is not empty")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters(
                            "SimulationParameters_EmptyOutputDirectory.xml"),
                    std::invalid_argument,
                    "Empty name");
        }

        SUBCASE("OutputDirectory is accepted")
        {
            SimulationParameters expectedParameters;
            expectedParameters.mOutDirectoryName = "OutputDirectory";
            auto parameters =
                    XMLParameters("SimulationParameters_OutputDirectory.xml");

            CHECK(expectedParameters == parameters.mSimulationParameters);
        }

        SUBCASE("OnlyEmPhysics is accepted")
        {
            SimulationParameters expectedParameters;
            expectedParameters.mOnlyEMPhysics = true;
            auto parameters =
                    XMLParameters("SimulationParameters_OnlyEmPhysics.xml");

            CHECK(expectedParameters == parameters.mSimulationParameters);
        }

#ifdef G4MULTITHREADED
        SUBCASE("NumberOfThreads is numeric")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("SimulationParameters_"
                                  "NumberOfThreadsIsntNumeric.xml"),
                    std::invalid_argument,
                    "NumberOfThreads is not numeric");
        }

        SUBCASE("NumberOfThreads is ≥ 1")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("SimulationParameters_"
                                  "NumberOfThreadsIsLessThanOne.xml"),
                    std::invalid_argument,
                    "NumberOfThreads is less than 1");
        }

        SUBCASE("NumberOfThreads is valid number")
        {
            SimulationParameters expectedParameters;
            expectedParameters.mNumberOfThreads = 2;
            auto parameters = XMLParameters(
                    "SimulationParameters_NumberOfThreadsIsValidNumber.xml");

            CHECK(expectedParameters == parameters.mSimulationParameters);
        }

        SUBCASE("NumberOfThreads is capped at cpu count")
        {
            SimulationParameters expectedParameters;
            expectedParameters.mNumberOfThreads = 4;
            auto parameters = XMLParameters(
                    "SimulationParameters_NumberOfThreadsIsCapped.xml");

            CHECK(expectedParameters == parameters.mSimulationParameters);
        }
#endif

        SUBCASE("Unknown key")
        {
            CHECK_THROWS_AS_MESSAGE(
                    XMLParameters("SimulationParameters_UnknownKey.xml"),
                    std::invalid_argument,
                    "Unknown key: FullWidthHalfMaximum");
        }

        SUBCASE("Complete example")
        {
            SimulationParameters expectedParameters;
            expectedParameters.mSimulationName = "Name";
            expectedParameters.mOutDirectoryName = "Directory";
            expectedParameters.mOnlyEMPhysics = true;
            auto parseResult = XMLParameters("SimulationParameters.xml");

            CHECK(expectedParameters == parseResult.mSimulationParameters);
        }

        SUBCASE("Others are kept at default")
        {
            auto parseResult = XMLParameters("SimulationParameters.xml");

            CHECK(parseResult.mBeamParameters == defaultBeam);
            CHECK(parseResult.mPhantomParameters == defaultPhantom);
            CHECK(parseResult.mWorldParameters == defaultWorld);
            CHECK(parseResult.mDetectorParameters.empty());
        }
    }

    SUBCASE("Unknown key")
    {
        CHECK_THROWS_AS_MESSAGE(XMLParameters("UnknownKey.xml"),
                                std::invalid_argument,
                                "Unknown key: FullWidthHalfMaximum");
    }
}
