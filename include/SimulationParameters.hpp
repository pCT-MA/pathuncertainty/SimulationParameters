#pragma once

#include <array>
#include <string>
#include <vector>

namespace Path
{
    constexpr const char* gAirMaterialName = "G4_AIR";
    constexpr const char* gSiliciumMaterialName = "G4_Si";
    constexpr const char* gWaterMaterialName = "G4_WATER";
    constexpr const char* gGalacticMaterialName = "G4_Galactic";

    constexpr const char* gProtonName = "proton";
    constexpr const char* gElectronName = "electron";
    constexpr const char* gHeliumName = "helium";
    constexpr std::array<const char*, 3> gSupportedParticles = {
            gProtonName, gElectronName, gHeliumName};

    struct BeamParameters
    {
        BeamParameters();

        std::string mParticleName;

        float mSourceZ;
        float mEnergy;
        float mFullWidthHalfMaximumInMM;

        std::size_t mNumberOfPrimaries;

        bool ParticleIsIon() const;
        int IonAtomicNumber() const;
        int IonMassNumber() const;
        double RestEnergy() const;
        /** @todo Does this play an important role? Right now this function
         * simply falls back to the atomic number. */
        int IonCharge() const;
        bool operator==(BeamParameters const& other) const;
    };

    struct VolumeParameters
    {
        VolumeParameters(std::string const& name, std::string const& material);
        VolumeParameters() = delete;

        std::string mName;
        std::string mMaterial;
        bool operator==(VolumeParameters const& other) const;
    };

    struct DetectorParameters : public VolumeParameters
    {
        DetectorParameters();

        float mMaterialBudget;
        float mPlacementZ;

        /** @brief Calculates the material thickness from the name and budget.*/
        float DetectorThickness() const;
        bool operator==(DetectorParameters const& other) const;
    };

    struct PhantomParameters : public VolumeParameters
    {
        PhantomParameters();

        float mThickness;
        std::size_t mNumberOfLayers;

        float LayerThickness() const;
        bool operator==(PhantomParameters const& other) const;
    };

    struct WorldParameters : public VolumeParameters
    {
        WorldParameters();

        float mWorldSize;

        bool operator==(WorldParameters const& other) const;
    };

    struct SimulationParameters
    {
        SimulationParameters();

        std::string mSimulationName;
        std::string mOutDirectoryName;

        bool mOnlyEMPhysics;

#ifdef G4MULTITHREADED
        std::size_t mNumberOfThreads;
#endif

        std::string SimulationFileName() const;

        bool operator==(SimulationParameters const& other) const;
    };

    struct ParameterSet
    {
        BeamParameters mBeamParameters;
        std::vector<DetectorParameters> mDetectorParameters;
        PhantomParameters mPhantomParameters;
        WorldParameters mWorldParameters;
        SimulationParameters mSimulationParameters;

        bool operator==(ParameterSet const& other) const;
    };

    ParameterSet XMLParameters(std::string const& fileName);
} // namespace Path
